# Tibber test task

## Quick start

```
 # build the container with defaults
 docker-compose build

 # build the container with custom database
 docker-compose build --build-arg DB=tibber.db

 # run the container
 docker-compose run -p 3001:3001 tibber npm start

 # running with up
 docker-compose up --build 

 #running with up and custom database
 DB=tibber.db docker-compose up --build 

```

Run tests only

```
docker-compose run tibber npm test
```

## Usage

POST to following API:

```
localhost:3001/tibber-developer-test/enter-path
```

with example body:

```
{
  "start": {
    "x": 10,
    "y": 22
  },
  "commmands": [
    {
      "direction": "west",
      "steps": 5
    },
     {
      "direction": "east",
      "steps": 5
    }
    ]
}
```

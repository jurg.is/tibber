import express from 'express'
import bodyParser from 'body-parser'
import { parseData } from './lib/parseData'
import { saveInDb } from './lib/saveInDb'

const PORT = 3001

const app = express()
app.use(bodyParser.json())

app.post('/tibber-developer-test/enter-path', async (req, res) => {
  const parsedResult = parseData(req.body)
  const result = await saveInDb(parsedResult)
  res.json(result)
})

app.listen(PORT, () => {
  console.log(`App is running on ${PORT}`)
})


FROM node:16.13.2 AS builder
ARG DB

RUN apt-get update && apt-get install -y sqlite3

WORKDIR /tibber

COPY package.json .

RUN npm install

ADD package.json ./
ADD ./lib ./lib
ADD index.ts ./
ADD types.ts ./
ADD tsconfig.json ./
ADD jest.config.js ./

RUN npm test

ENV DB=$DB
RUN npm run build


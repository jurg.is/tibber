export enum DIRECTION {
  EAST = 'east',
  NOTRH = 'north',
  WEST = 'west',
  SOUTH = 'south',
}

export type Position = {
  x: number
  y: number
}

export type Command = {
  start: {
    x: number
    y: number
  }
  commmands: {
    direction: DIRECTION
    steps: number
  }[]
}

export type Result = {
  commands: number
  result: number
  duration: number
}

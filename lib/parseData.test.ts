import { describe, test, expect } from '@jest/globals'
import { Command, DIRECTION } from '../types'
import { parseData } from './parseData'

const testCommand: Command = {
  start: {
    x: 5,
    y: 5,
  },
  commmands: [
    // move 5 up
    {
      direction: DIRECTION.NOTRH,
      steps: 5,
    },
    // move 2 back down
    {
      direction: DIRECTION.SOUTH,
      steps: 2,
    },
    // move 3 left
    {
      direction: DIRECTION.WEST,
      steps: 3,
    },
    // move 50 right
    {
      direction: DIRECTION.EAST,
      steps: 3,
    },
  ],
}

describe('Test complete data set of commands', () => {
  test('parse incoming data', () => {
    const res = parseData(testCommand)
    expect(res.commands).toEqual(4)
    // unique places visited
    expect(res.result).toEqual(9)
  })
})

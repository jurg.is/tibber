import { Command } from '../types'
import { moveStep } from './moveStep'

export const parseData = (data: Command) => {
  const start = performance.now()
  const visitedCoords = new Map<string, boolean>()
  // set start coordinations as visited and set it as starting point
  visitedCoords.set(`${data.start.x}-${data.start.y}`, true)
  let current = data.start

  data.commmands.forEach((item) => {
    for (let i = 0; i < item.steps; i++) {
      current = moveStep(current, item.direction)
      visitedCoords.set(`${current.x}-${current.y}`, true)
    }
  })
  // calculations duration
  const end = performance.now()
  const seconds = (end - start) / 1000

  return {
    commands: data.commmands.length,
    result: visitedCoords.size,
    duration: seconds,
  }
}

import { describe, test, expect } from '@jest/globals'

import { DIRECTION } from '../types'
import { moveStep } from './moveStep'

describe('test moveStep for each direction while keeping previous result', () => {
let res = { x: 0, y: 0 }
  test('move east', () => {
    res = moveStep(res, DIRECTION.EAST)
    expect(res).toStrictEqual({ x: 1, y: 0 })
  })
  test('move north', () => {
    res = moveStep(res, DIRECTION.NOTRH)
    expect(res).toStrictEqual({ x: 1, y: 1 })
  })
  test('move west', () => {
    res = moveStep(res, DIRECTION.WEST)
    expect(res).toStrictEqual({ x: 0, y: 1 })
  })
  test('move south', () => {
    res = moveStep(res, DIRECTION.SOUTH)
    expect(res).toStrictEqual({ x: 0, y: 0 })
  })
})

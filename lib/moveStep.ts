import { DIRECTION, Position } from '../types'

export const moveStep = (position: Position, direction: DIRECTION): Position => {
  const { x, y } = position
  switch (direction) {
    case DIRECTION.EAST: {
      return {
        y,
        x: x + 1,
      }
    }
    case DIRECTION.NOTRH: {
      return {
        y: y + 1,
        x,
      }
    }
    case DIRECTION.WEST: {
      return {
        y,
        x: x - 1,
      }
    }
    case DIRECTION.SOUTH: {
      return {
        y: y - 1,
        x,
      }
    }
  }
}

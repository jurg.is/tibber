import sqlite3 from 'sqlite3'
import { Result } from '../types'

const dbName = process.env.DB || 'commands.db'
const db = new sqlite3.Database(dbName)

db.run(`
    CREATE TABLE IF NOT EXISTS results (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        timestamp DATE,
        commands INT,
        result INT,
        duration REAL
    )   
    `)

export const saveInDb = ({ commands, duration, result }: Result) => {
  return new Promise((resolve) => {
    const timestamp = new Date().toISOString()
    db.run(
      `
            INSERT INTO 
            results(timestamp, commands, result, duration) 
            VALUES($timestamp, $commands, $result, $duration)
       `,
      {
        $timestamp: timestamp,
        $commands: commands,
        $result: result,
        $duration: duration,
      },
      function (err) {
        if (err !== null) {
          throw new Error('Failed saving to database, exiting')
        }
        resolve({ commands, duration, result, timestamp, id: this.lastID })
      }
    )
  })
}
